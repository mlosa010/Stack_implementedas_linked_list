/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stacklinked.list;

import java.util.NoSuchElementException;



/**
 *
 * @author manuel
 */
public class LinkedListStack {
    
    
    private Node first;
    
    /**
     * constructs an empty stack
     */
    
    public LinkedListStack(){
        first =null;
    }
    /**
     * adds an element to the top of the stack
     * @param element the element that is supposed to be 
     * added
     */
    
    public void push(Object element)
    {
        Node newNode = new Node();
        newNode.data =element;
        newNode.next =first;
        first =newNode;
    }
    
    /**
     * 
     * @return the last object in the stack 
     */
    public Object pop()
    {
        if(first==null)
        {
            throw new NoSuchElementException();
        }
        Object element = first.data;
        first =first.next;
        System.out.println(first);
        return element;
    }
    public boolean empty(){
        return first == null;
    }
    /**
     * inner class represents the node
     */
    class Node{
        public Object data;
        public Node next;
    }
}
